using NUnit.Framework;
using StackLibrary;
using System;

namespace IdealStackTests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [TestCase(new int[10] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 })]
        public void CreateStackWithIntArray_ReturnTrueIfSuccesful(int [] array)
        {
            var stack = new IdealStack<int>(array);
            bool createdCorrectly = true;
            Array.Reverse(array);
            for (int i = 0; i < array.Length; i++)
            {
                if(array[i] != stack.Pop())
                {
                    createdCorrectly = false;
                }
            }
            Assert.AreEqual(true, createdCorrectly, message: "stack created incorrectly");
        }

        [TestCase(new char[10] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' })]
        public void CreateStackWithCharArray_ReturnTrueIfSuccesful(char[] array)
        {
            var stack = new IdealStack<char>(array);
            bool createdCorrectly = true;
            Array.Reverse(array);
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] != stack.Pop())
                {
                    createdCorrectly = false;
                }
            }
            Assert.AreEqual(true, createdCorrectly, message: "stack created incorrectly");
        }

        [TestCase(new int[2] { 0, 1}, new double[2] { 0.0, 2.3}, new string[2] { "one", "two"})]
        public void PushGivenIntElementsToStack_ReturnArrayOFStack(int [] array, double [] arrayD, string [] arrayS)
        {
            var stackInt = new IdealStack<int>();
            var stackDouble = new IdealStack<double>();
            var stackString = new IdealStack<string>();
            foreach (var item in array)
            {
                stackInt.Push(item);
            }
            foreach (var item in arrayD)
            {
                stackDouble.Push(item);
            }
            foreach (var item in arrayS)
            {
                stackString.Push(item);
            }
            Array.Reverse(array);
            Array.Reverse(arrayD);
            Array.Reverse(arrayS);
            Assert.AreEqual((array, arrayD, arrayS), (stackInt.ToArray(), stackDouble.ToArray(), stackString.ToArray()), message: "elements were not pushed corectly");
        }

        [TestCase(new int[10] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 })]
        public void PeeKElement_ReturnCorrectLastElementWithoutDeleting(int[] array)
        {
            var stack = new IdealStack<int>(array);
            int[] newArray = new int[array.Length];
            for (int i = 0; i < array.Length; i++)
            {
                newArray[i] = stack.Peek();
                stack.Pop();
            }
            Array.Reverse(array);
            Assert.AreEqual(array, newArray, message: "peek of elements works incorrectly");
        }

        [TestCase(new int[10] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }, 0)]
        public void PopElement_ReturnCorrectLastElementAndDeleteIt(int[] array, int expectedCount)
        {
            var stack = new IdealStack<int>(array);
            int [] newArray = new int[array.Length];
            for (int i = 0; i < array.Length; i++)
            {
                newArray[i] = stack.Pop();
            }
            int count = stack.Count;
            Array.Reverse(array);
            Assert.AreEqual((array, expectedCount), (newArray, count), message: "popping works incorrectly");
        }

        [TestCase(new int[10] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }, 0, 10)]
        public void Contains_ReturnTrueIfElementIsFinded(int[] array, int expectedForTrue, int expectedForFalse)
        {
            var stack = new IdealStack<int>(array);
            bool firstResult = stack.Contains(expectedForTrue);
            bool secondResult = stack.Contains(expectedForFalse);
            int count = stack.Count;
            Array.Reverse(array);
            Assert.AreEqual((true, false), (firstResult, secondResult), message: "searching of elements (contains) works incorrectly");
        }

        [TestCase(new int[10] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }, 0)]
        public void ClearStack_ReturnZeroCount(int[] array, int expectedCount)
        {
            var stack = new IdealStack<int>(array);
            stack.Clear();
            int Count = stack.Count;
            Assert.AreEqual(expectedCount, Count, message: "clearing of stack works incorrectly");
        }

        [TestCase(new int[10] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, new int[3] { 1, 2, 3}, new int[10] { 0, 0, 0, 3, 2, 1, 0, 0, 0, 0}, 3)]
        public void CopyTo_CopyStackToArrayOnGivenIndex_ReturnFinalArray(int[] startArray, int [] array, int [] finalArray, int index)
        {
            var stack = new IdealStack<int>(array);
            stack.CopyTo(startArray, index);
            Assert.AreEqual(finalArray, startArray, message: "coppying of stack works incorrectly");
        }

        [TestCase(new int[10] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 })]
        public void ToArray_ReturnStackAsArrayOfSomeType(int[] array)
        {
            var stack = new IdealStack<int>(array);
            int[] newArray = stack.ToArray();
            Array.Reverse(array);
            Assert.AreEqual(array, newArray, message: "converting of stack to array works incorrectly");
        }



        [Test]
        public void CreateStackWithNullArgument_ReturnArgumentException()
        {
            int[] nullArray = null;
            var expectedException = typeof(ArgumentException);
            var actException = Assert.Catch(() => new IdealStack<int>(nullArray));
            Assert.AreEqual(expectedException, actException.GetType(), message: "stack can't be created with null argument");
        }

        [Test]
        public void PushNullElement_ReturnArgumentNullException()
        {
            var stack = new IdealStack<object>();
            object obj = null;
            var expectedException = typeof(ArgumentNullException);
            var actException = Assert.Catch(() => stack.Push(obj));
            Assert.AreEqual(expectedException, actException.GetType(), message: "null element can't be pushed");
        }

        [Test]
        public void PeekElementFromEmptyStack_ReturnInvalidOperationException()
        {
            var stack = new IdealStack<object>();
            var expectedException = typeof(InvalidOperationException);
            var actException = Assert.Catch(() => stack.Peek());
            Assert.AreEqual(expectedException, actException.GetType(), message: "There is no element to peek in empty stack");
        }

        [Test]
        public void PopElementFromEmptyStack_ReturnInvalidOperationException()
        {
            var stack = new IdealStack<object>();
            var expectedException = typeof(InvalidOperationException);
            var actException = Assert.Catch(() => stack.Pop());
            Assert.AreEqual(expectedException, actException.GetType(), message: "There is no element to pop in empty stack");
        }

        [Test]
        public void ContainsWithNullElement_ReturnArgumentNullException()
        {
            var stack = new IdealStack<object>();
            object obj = null;
            var expectedException = typeof(ArgumentNullException);
            var actException = Assert.Catch(() => stack.Contains(obj));
            Assert.AreEqual(expectedException, actException.GetType(), message: "null element can't be found");
        }

        [Test]
        public void CopyToNullArray_ReturnArgumentNullException()
        {
            var stack = new IdealStack<int>(new int [5] { 1, 4, 6, 3, 5 });
            int[] nullArray = null;
            var expectedException = typeof(ArgumentNullException);
            var actException = Assert.Catch(() => stack.CopyTo(nullArray, 1));
            Assert.AreEqual(expectedException, actException.GetType(), message: "stack can't be coppied to null array");
        }

        [Test]
        public void CopyToArrayOnNegativeIndex_ReturnArgumentOutOfRangeException()
        {
            var stack = new IdealStack<int>(new int[5] { 1, 4, 6, 3, 5 });
            int[] array = new int[10];
            var expectedException = typeof(ArgumentOutOfRangeException);
            var actException = Assert.Catch(() => stack.CopyTo(array, -1));
            Assert.AreEqual(expectedException, actException.GetType(), message: "stack can't be coppied to array on negative index");
        }

        [Test]
        public void CopyToArrayOnIdexWhereStackWillGoOutOfArrayLength_ReturnArgumentException()
        {
            var stack = new IdealStack<int>(new int[5] { 1, 4, 6, 3, 5 });
            int[] array = new int[10];
            var expectedException = typeof(ArgumentException);
            var actException = Assert.Catch(() => stack.CopyTo(array, 8));
            Assert.AreEqual(expectedException, actException.GetType(), message: "stack can't be coppied to null array");
        }
    }
}