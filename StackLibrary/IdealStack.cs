﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace StackLibrary
{
    public class IdealStack<T> : IEnumerable<T>
    {
        /// <summary>
        /// The "head" of stack - first element of some type
        /// </summary>
        Node<T> Top = null;
        /// <summary>
        /// This property show the amount of elements in the stack
        /// </summary>
        public int Count { get; private set; }
        /// <summary>
        /// The initialization of delegate for event for the stack
        /// </summary>
        /// <param name="message">the string that will be shown as message of event</param>
        public delegate void StackEvents(object obj, EventArgs<T> arguments);
        /// <summary>
        /// The initialization of event function that will call for event work
        /// </summary>
        public event StackEvents Pushed;
        public event StackEvents Popped;
        public event StackEvents Cleared;
        public event StackEvents ConvertedToArray;
        public event StackEvents Coppied;

        /// <summary>
        /// Constructor for creating stack without parameters
        /// </summary>
        public IdealStack()
        {
        }
        /// <summary>
        /// The constructor for creating stack from given collection
        /// </summary>
        /// <param name="collection">Any collection of elements of some type</param>
        /// <exception cref="ArgumentException">throws exception when there is no collection in called method</exception>
        public IdealStack(ICollection<T> collection)
        {
            if (collection == null)
            {
                throw new ArgumentException();
            }
            foreach (var item in collection)
            {
                Push(item);
            }
        }
        /// <summary>
        /// This method adds an element given by user to stack
        /// </summary>
        /// <param name="item">the element of some type that is wanted to add by user</param>
        /// <exception cref="ArgumentNullException">throws exception when there is no given element to push</exception>
        public void Push(T item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            Node<T> Node = new Node<T>(item);
            if (Top == null)
            {
                Top = Node;
            }
            else
            {
                Node.NextNode = Top;
                Top = Node;
            }
            Count++;
            Pushed?.Invoke(this, new EventArgs<T>("Element pushed", item));
        }
        /// <summary>
        /// Return first element of the stack without deleting it
        /// </summary>
        /// <exception cref="InvalidOperationException">throws exception if there isn't any elements in stack</exception>
        /// <returns>return the value of first element</returns>
        public T Peek()
        {
            if (Top == null)
            {
                throw new InvalidOperationException();
            }
            return Top.Value;
        }
        /// <summary>
        /// Return first element of the stack and deletes it
        /// </summary>
        /// <exception cref="InvalidOperationException">throws exception if there isn't any elements in stack</exception>
        /// <returns>return the value of first element</returns>
        public T Pop()
        {
            if (Top == null)
            {
                throw new InvalidOperationException();
            }
            T Poped = Top.Value;
            Top = Top.NextNode;
            Count--;
            Popped?.Invoke(this, new EventArgs<T>("Element popped", Poped));
            return Poped;
        }
        /// <summary>
        /// The method checks if there is an element given by user in the stack
        /// </summary>
        /// <param name="item">the element of some type that is finding by method</param>
        /// <exception cref="ArgumentNullException">throws exception if given element is null</exception>
        /// <returns>true, if element is found, or false, if isn't</returns>
        public bool Contains(T item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            Node<T> CurrentNode = Top;
            while (CurrentNode != null)
            {
                if (Top != null)
                {
                    if (CurrentNode.Value.Equals(item))
                    {
                        return true;
                    }
                    else
                    {
                        CurrentNode = CurrentNode.NextNode;
                    }
                }
            }
            return false;
        }
        /// <summary>
        /// Returns enumarator that itarete throught stack
        /// </summary>
        /// <returns>values of elements for iterating</returns>
        public IEnumerator<T> GetEnumerator()
        {
            Node<T> CurrentNode = Top;
            while (CurrentNode != null)
            {
                yield return CurrentNode.Value;
                CurrentNode = CurrentNode.NextNode;
            }
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        /// <summary>
        /// The method that delete all elements in stack
        /// </summary>
        public void Clear()
        {
            Top = null;
            Count = 0;
            Cleared?.Invoke(this, new EventArgs<T>("Element pushed"));
        }
        /// <summary>
        /// The method coppies the stack to an array on position with given index
        /// </summary>
        /// <param name="Array">array where stack will be coppied</param>
        /// <param name="Index">index of position where will be copppied the stack</param>
        public void CopyTo(T[] Array, int Index)
        {
            if (Array == null)
            {
                throw new ArgumentNullException();
            }
            if (Index < 0)
            {
                throw new ArgumentOutOfRangeException();
            }
            if (Index + Count > Array.Length)
            {
                throw new ArgumentException();
            }
            var addArray = ToArray();
            foreach (var item in addArray)
            {
                Array[Index++] = item;
            }
            string[] ArrayToString = new string[Array.Length];
            for (int i = 0; i < Array.Length; i++)
            {
                ArrayToString[i] = Array[i].ToString();
            }
            Coppied?.Invoke(this, new EventArgs<T>("Stack was coppied to array:", ArrayToString));
        }
        /// <summary>
        /// The method converts the stack to array
        /// </summary>
        /// <returns>new converted array</returns>
        public T[] ToArray()
        {
            Node<T> CurrentNode = Top;
            T[] Array = new T[Count];
            int Index = 0;
            while (CurrentNode != null)
            {
                Array[Index++] = CurrentNode.Value;
                CurrentNode = CurrentNode.NextNode;
            }
            string[] ArrayToString = new string[Array.Length];
            for (int i = 0; i < Array.Length; i++)
            {
                ArrayToString[i] = Array[i].ToString();
            }
            ConvertedToArray?.Invoke(this, new EventArgs<T>("Stack was converted to array", ArrayToString));
            return Array;
        }
    }
}
