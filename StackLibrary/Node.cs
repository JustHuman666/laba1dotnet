﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StackLibrary
{
    public class Node<T>
    {
        /// <summary>
        /// This property show the value of node
        /// </summary>
        public T Value { get; set; }
        /// <summary>
        /// This property save the information about next node in stack
        /// </summary>
        public Node<T> NextNode = null;
        /// <summary>
        /// This constructor set value for node
        /// </summary>
        /// <param name="Value">any value of some type "T" for node</param>
        public Node(T Value)
        {
            this.Value = Value;
        }
    }
}
