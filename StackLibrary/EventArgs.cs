﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StackLibrary
{
    public class EventArgs<T>
    {
        public string message { get; private set; }
        public T value { get; private set; }
        public string [] array { get; private set; }
        public EventArgs(string message)
        {
            this.message = message;
        }
        public EventArgs(string message, T value)
        {
            this.message = message;
            this.value = value;
        }
        public EventArgs(string message, string [] array)
        {
            this.message = message;
            this.array = array;
        }
    }
}
